@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <form action="" class="form-inline">
                <div class="form-group row">
                    <div class="col-md-6 d-flex">
                        <input class="form-control" name="key"  placeholder="Name" class="form-control">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>

                    <div class="col-md-6">
                    </div>
                </div>
            </form>

        </div>

        <br>
        <th> <a href="{{ route('user.create') }}" class="btn btn-primary">Create</a>
        </th>

        <table class="table table-striped">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Is admin</th>
                <th>Action</th>


            </tr>
            @foreach($users as $user)
                <tr>
                    <th>{{ $user->id }}</th>
                    <th>{{ $user->name  }}</th>
                    <th>{{ $user->email }}</th>
                    <th>{{ $user->is_admin ? "x" : "" }}</th>

                    <th>
                        <form method="POST" action="{{ route('user.destroy', $user->id) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-info">Delete</button>
                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-secondary">Edit</a>
                        </form>
                    </th>
                </tr>
            @endforeach()
        </table>

        {{ $users->links() }}
    </div>
    </div>
@endsection

