<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoleDeleteTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $this->assertDatabaseMissing('roles', ['id' => $role->id]);

        $response->assertRedirect($this->getListRoleRoute());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertRedirect('/login');
    }

    public function getDeleteRoleRoute($id)
    {
        return route('roles.destroy', $id);
    }

    public function getListRoleRoute()
    {
        return route('roles.index');
    }
}
