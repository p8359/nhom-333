<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserDeleteTest extends TestCase
{
    public function getDeleteUserRoute($id)
    {
        return route('user.destroy',$id);
    }
    public function test_auth_can_delete_user()
    {
        $admin = $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $response=$this->delete($this->getDeleteUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
    public function test_unauth_can_not_delete_user()
    {
        $users = User::factory()->create();
        $response=$this->delete($this->getDeleteUserRoute($users->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }
}
