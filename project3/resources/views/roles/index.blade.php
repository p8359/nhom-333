
@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <form action="" class="form-inline">
                <div class="form-group row">
                    <div class="col-md-6 d-flex">
                        <input class="form-control" name="key"  placeholder="Name" class="form-control">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>

                    <div class="col-md-6">
                    </div>
                </div>
            </form>

        </div>

        <br>
        <th> <a href="{{ route('roles.create') }}" class="btn btn-primary">Create</a>
</th>

            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>


                </tr>
                @foreach($roles as $role)
                    <tr>
                        <th>{{ $role->id }}</th>
                        <th>{{ $role->name  }}</th>
                        <th>{{ $role->email }}</th>
                        <th>
                            <img src="..." class="img-thumbnail" alt="...">
                        </th>
                        <th>

                            <form method="POST" action="{{ route('roles.destroy', $role->id) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-info">Delete</button>
                                <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-secondary">Edit</a>
                                 </form>
                        </th>
                    </tr>
                @endforeach()
            </table>

            {{ $roles->appends(request()->all())->links() }}
        </div>
    </div>
    @endsection
