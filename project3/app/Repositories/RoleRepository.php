<?php

namespace App\Repositories;


use App\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleRepository implements IRepository
{
    private $role ;
    public function __construct(Role $role)
    {
        $this->role=$role;
    }

    public function paginate()
    {
        return $this->role->orderBy('id','DESC')->Search()->paginate(5);

    }

    public function all()
    {
        return $this->role->all();
    }

    public function create($data)
    {
        return $this->role->create(["name" => $data["name"]])->syncPermissions($data['permission']);
    }

    public function update($data, $id)
    {
        return $this->role->find($id)->syncPermissions($data['permission'])->update($data);
    }

    public function delete($id)
    {
        return $this->role->find($id)->delete();
    }

    public function show($id)
    {
        return $this->role->find($id);

    }
}
