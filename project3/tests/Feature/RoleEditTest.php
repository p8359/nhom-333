<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoleEditTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_edit_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->create();
        $response = $this->put($this->getEditRoleRoute($role->id), $role->toArray());
        $response->assertStatus(302);
        $this->assertDatabaseHas('roles', $role->toArray());
        $response->assertRedirect(route('roles.index'));
    }
    /** @test */
    public function unauthenticated_user_can_not_edit_role()
    {
        $role = Role::factory()->create();
        $response = $this->put($this->getEditRoleRoute($role->id), $role->toArray());
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_user_can_not_edit_role_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'email' => $role->email
        ];
        $response = $this->put($this->getEditRoleRoute($role->id), $data);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function unnauthenticated_user_can_not_see_edit_role_form_view()
    {
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'email' => $role->email
        ];
        $response = $this->get($this->getEditRoleViewRoute($role->id),$data);
        $response->assertRedirect('/login');
    }

    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function getEditRoleRoute($id)
    {
        return route('roles.update',$id);
    }
    public function getEditRoleViewRoute($id)
    {
        return route('roles.edit',$id);
    }
}
