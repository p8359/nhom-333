@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Edit roles</h2>
                <form action="{{ route('roles.update', $role->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" value="{{ $role->name }}" name="name" placeholder="Name ..." size="50" >
                            @error('name')
                            <span id="name-error" class="error text-danger"
                                  style="...">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $role->content }}" name="email" placeholder="Email ..." size="50">
                            @error('email')
                            <span id="name-error" class="error text-danger"
                                  style="...">{{ $message }}</span>
                            @enderror
                        </div>
                        <button class="btn btn-success">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
