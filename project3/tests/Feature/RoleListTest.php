<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Tests\TestCase;

class RoleListTest extends TestCase
{
    public function getListRoleRoute()
    {
        return route('roles.index');
    }
    /** @test */
    public function role_can_get_all()
    {
        $user =User::factory()->create();
        $this->actingAs($user);
        $role = Role::factory()->create();
        $response = $this->get($this->getListRoleRoute());

        $response->assertStatus(200);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->name);
    }
    public function test_unauth_can_not_view_list_role()
    {
        $response =$this->get($this->getListRoleRoute());
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

}
