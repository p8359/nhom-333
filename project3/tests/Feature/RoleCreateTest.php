<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleCreateTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_new_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $role);

        $response->assertStatus(302);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect(route('roles.index'));
    }


    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $role);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticated_user_can_not_create_role_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getCreateRoleRoute(), $role);
        $response->assertSessionHasErrors(['name']);
    }
    /** @test */
    public function authenticated_user_can_view_create_role_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateRoleViewRoute());
        $response->assertViewIs('roles.create');
    }

    /** @test */
    public function authenticated_user_can_see_name_required_text_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->make(['name' => null])->toArray();
        $respose = $this->from($this->getCreateRoleViewRoute())->post($this->getCreateRoleRoute(), $role);
        $respose->assertRedirect($this->getCreateRoleViewRoute());

    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_role_form_view()
    {
          $respose = $this->get($this->getCreateRoleViewRoute());
        $respose->assertRedirect('/login');
    }

    public function getCreateRoleRoute()
    {
        return route('roles.store');
    }
    public function getCreateRoleViewRoute()
    {
        return route('roles.create');
    }
}
