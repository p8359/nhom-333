<?php

namespace App\Services;

interface IService
{
    public function paginate();

    public function create($data);

    public function update($data, $id);

    public function delete($id);

    public function show($id);

    public function all();
}
