<?php

use App\Http\Controllers\RoleController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});
Route::get('/roles', [RoleController::class, 'index'])
    ->name('roles.index')->middleware('auth');

Route::post('/roles', [RoleController::class, 'store'])
    ->name('roles.store')
    ->middleware('auth');

Route::get('/roles/create', [RoleController::class, 'create'])
    ->name('roles.create')
    ->middleware('auth');

Route::get('/roles/edit/{id}', [RoleController::class, 'edit'])
    ->name('roles.edit')
    ->middleware('auth');

Route::put('/roles/update/{id}', [RoleController::class, 'update'])
    ->name('roles.update')
    ->middleware('auth');

Route::delete('/roles/destroy/{id}', [RoleController::class, 'destroy'])
    ->name('roles.destroy')
    ->middleware('auth');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('user', UserController::class);
});

//Route::get('/user', [UserController::class, 'index'])
//    ->name('user.index');
//
//Route::post('/user', [UserController::class, 'store'])
//    ->name('user.store')
//    ->middleware('auth');
//
//Route::get('/user/create', [UserController::class, 'create'])
//    ->name('user.create')
//    ->middleware('auth');
//
//Route::get('/user/edit/{id}', [UserController::class, 'edit'])
//    ->name('user.edit')
//    ->middleware('auth');
//
//Route::put('/user/update/{id}', [UserController::class, 'update'])
//    ->name('user.update')
//    ->middleware('auth');
//
//Route::delete('/user/destroy/{id}', [UserController::class, 'destroy'])
//    ->name('user.destroy')
//    ->middleware('auth');


    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource("/product", ProductController::class)->middleware('auth');

