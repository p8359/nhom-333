
@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Thêm người dùng</h2>
                <form action=" {{ route('user.store') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" name="name" placeholder="Name ...">
                            @error('name')
                            <span id="name-error" class="error text-danger"
                                  style="display: block;">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="email" class="form-group" name="email" placeholder="Email ...">
                        </div>
                        <div class="card-body">
                            <input type="password" class="form-group" name="password" placeholder="please enter password ...">
                        </div>
                        <div class="card-body">
                            <input type="confirm" class="form-group" name="confirm" placeholder="please confirm password ...">
                        </div>
                        <div class="card-body">
                            <label>Is_admin</label>
                            <label class="radio-inline">
                                <input name="is_admin" value="0" checked type="radio">User
                            </label>
                            <label class="radio-inline">
                                <input name="is_admin" value="1" checked type="radio">Admin

                            </label>
                        </div>

                        <button class="btn btn-success">Thêm mới</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
