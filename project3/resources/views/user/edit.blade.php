
@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>edit</h2>
                @if(count($errors))
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{ $err }}
                        @endforeach
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
                <form action=" {{ route('user.update', $user->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" value="{{ $user->name }}" name="name" placeholder="Name ...">
                            @error('name')
                            <span id="name-error" class="error text-danger"
                                  style="display: block;">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="email" class="form-group" name="email" placeholder="Email ..." readonly>
                        </div>
                        <div class="card-body">
                            <input type="password" class="form-group" name="password" placeholder="please enter password ...">
                        </div>
                        <div class="card-body">
                            <input type="confirm" class="form-group" name="confirm" placeholder="please confirm password ...">
                        </div>
                        <div class="card-body">
                            <label>Is_admin</label>
                            <label class="radio-inline">
                                <input name="is_admin" value="0" @if(!$user->is_admin) checked @endif type="radio">User
                            </label>
                            <label class="radio-inline">
                                <input name="is_admin" value="1" @if($user->is_admin) checked @endif type="radio">Admin

                            </label>
                        </div>

                        <button class="btn btn-success">update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
