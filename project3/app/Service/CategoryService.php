<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryService implements IService
{
    private $repo;
    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }

    public function paginate()
    {
        try {
            return $this->repo->paginate();
        }
        catch (ModelNotFoundException $exception){
            return back()->withError($exception->getMessage())->withInput();
        }

    }
    public function all()
    {
        try {
            return $this->repo->all();
        }
        catch (ModelNotFoundException $exception){
            return back()->withError($exception->getMessage())->withInput();
        }

    }
    public function show($id)
    {
        try {
            return $this->repo->show($id);
        }
        catch (ModelNotFoundException $exception){
            return null;
        }
    }
    public function update($data, $id)
    {
        try {
            $this->repo->update($data,$id);
            return true;
        }
        catch (ModelNotFoundException $exception){
            return false;
        }
    }
    public function create($data)
    {
        try {
            $this->repo->create($data);
            return true;
        }
        catch (ModelNotFoundException $exception){
            return false;
        }
    }
    public function delete($id)
    {
        try {

            return $this->repo->delete($id);
        }
        catch (ModelNotFoundException $exception){
            return false;
        }
    }

}
