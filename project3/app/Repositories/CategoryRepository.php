<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository implements IRepository
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function paginate()
    {
        return $this->category->latest()->Search()->paginate(3);
    }
    public function all()
    {
        return $this->category->all();
    }
    public function update( $data, $id)
    {
        return $this->category->findOrFail($id)->update($data);
    }

    public function create( $data)
    {
        return $this->category->create($data);
    }
    public function delete($id)
    {
        return $this->category->findOrFail($id)->delete();
    }
    public function show($id)
    {
        return $this->category->findOrFail($id);
    }
}
