<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $User;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index()
    {
        $users = $this->user->latest('id')->paginate(5);
        return view('user.list', compact('users'));
    }

    public function create()
    {
        return view('user.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6|max:64',
            'confirm' => 'same:password',
            'is_admin' => 'required'
        ]);

        User::create([
           'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'is_admin' => $request->is_admin,
        ]);

        return redirect()->route('user.index')->with('success', 'Created successfully');
    }
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'is_admin' => 'required',
        ]);
        $user = User::find($id);
        $data = [
            'name' => $request->name,
            'is_admin' => $request->is_admin
        ];
        if ($request->password){
            $this->validate($request, [
                'password' => 'required|min:6|max:64',
                'confirm' => 'same:password',
            ]);
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);
        return redirect()->route('user.edit', $user->id)->with('success', 'updated successfully');
    }
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('user.index')->with('success', 'deleted successfully');
    }
}
