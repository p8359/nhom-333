<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserEditTest extends TestCase
{
    public function getEditUserViewRoute($id)
    {
        return route('user.edit',$id);
    }

    public function getEditUserRoute($id)
    {
        return route('user.update',$id);
    }

    public function test_auth_can_update_user()
    {
        $admin = $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }
    public function test_unauth_can_not_update_user()
    {
        $user = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    public function test_auth_can_see_update_user()
    {
        $admin = $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $response=$this->get($this->getEditUserViewRoute($user->id));
        $response->assertViewIs('user.edit');
    }
    public function test_unauth_can_not_see_update_user()
    {
        $user = User::factory()->create();
        $response=$this->get($this->getEditUserViewRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    public function test_auth_can_see_validate_update_user()
    {
        $admin = $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $user->name = null;
        $response = $this->from($this->getEditUserViewRoute($user->id))->put($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }
}
