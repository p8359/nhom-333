<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserListTest extends TestCase
{
    public function getListUserRoute()
    {
        return route('user.index');
    }
    /** @test */
    public function user_can_get_all()
    {
        $user =User::factory()->create();
        $this->actingAs($user);
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());

        $response->assertStatus(200);
        $response->assertViewIs('user.list');
        $response->assertSee($user->name);
    }

}
