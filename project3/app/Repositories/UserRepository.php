<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;
use App\Models\User;

class UserRepository implements IRepository
{
    private $user ;
    public function __construct(User $user)
    {
        $this->user=$user;

    }

    public function paginate()
    {
        return $this->user->Role()->orderBy('id','DESC')->Search()->paginate(5);
    }

    public function all()
    {
        return $this->user->all();
    }

    public function create($data)
    {
        return $this->user->create($data);
    }

    public function update($data, $id)
    {
        return $this->user->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->user->find($id)->delete();
    }

    public function show($id)
    {
        return $this->user->find($id);
    }
}
