<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserCreateTest extends TestCase
{
    public function getCreateUserViewRoute()
    {
        return route('user.create');
    }

    public function getCreateUserRoute()
    {
        return route('user.store');
    }

    public function test_unauth_can_not_view_create_user()
    {
        $response =$this->get($this->getCreateUserViewRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    public function test_auth_can_view_create_user()
    {
        $admin = $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateUserViewRoute());
        $response->assertViewIs('user.create');
    }
    public function test_unauth_can_not_create_user()
    {
        $response =$this->post($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    public function test_auth_can_create_user()
    {
        $admin= $this->actingAs(User::factory()->create());
        $users=User::factory()->make();
        $response = $this->post($this->getCreateUserRoute(),$users->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }
public function test_auth_can_see_validate_create_user()
{
    $admin = $this->actingAs(User::factory()->create());
    $user= User::factory()->make();
    $user ->name =null;
    $response = $this->post($this->getCreateUserRoute(), $user->toArray());
    $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
}
}
