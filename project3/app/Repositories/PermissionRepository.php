<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;

class PermissionRepository implements IRepository
{
    private $permission ;
    public function __construct(Permission $permission)
    {
        $this->permission=$permission;

    }

    public function paginate()
    {
        return $this->permission->orderBy('id','DESC')->Search()->paginate(5);
    }

    public function all()
    {
        return $this->permission->all();
    }

    public function create($data)
    {
        // TODO: Implement create() method.
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        return $this->permission->find($id);
    }
}
