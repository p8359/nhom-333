<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    protected $Role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->latest('id')->search()->paginate(10);
        return view('roles.index', compact('roles'));
    }
    public function store(CreateRoleRequest $request)
    {
        $this->role->create($request->all());
        return redirect()->route('roles.index');
    }

    public function edit($id)
    {
        $role = $this->role->findOrFail($id);
        return view('roles.edit',compact("role"));
    }

    public function update(CreateRoleRequest $request, $id)
    {
        $article =$this->role->findOrFail($id);
        $article->update($request->all());
        return redirect()->route('roles.index');
    }
    public function destroy($id)
    {
        $article =$this->role->findOrFail($id);
        $article->delete();
        return redirect()->route('roles.index');
    }

    public function create()
    {
        return view('roles.create');
    }
}
